require essioc
require caenelsmagnetps, 1.2.2+1
require magnetps, 1.1.0

############################################################################
# ESS IOC configuration
############################################################################
epicsEnvSet("IOCNAME", "MEBT-010:SC-IOC-008")
epicsEnvSet("IOCDIR", "MEBT-010_SC-IOC-008")
iocshLoad("$(essioc_DIR)/common_config.iocsh")
#
############################################################################
# MEBT Magnets IP addresses
############################################################################
iocshLoad("$(E3_CMD_TOP)/fastps_ips.iocsh")
#
#
############################################################################
# Load conversion break point table
############################################################################
dbLoadDatabase("MEBT-010_BMD-CH-001.dbd", "$(magnetps_DB)/", "")
dbLoadDatabase("MEBT-010_BMD-CH-002.dbd", "$(magnetps_DB)/", "")
dbLoadDatabase("MEBT-010_BMD-CH-003.dbd", "$(magnetps_DB)/", "")
dbLoadDatabase("MEBT-010_BMD-CH-004.dbd", "$(magnetps_DB)/", "")
dbLoadDatabase("MEBT-010_BMD-CH-005.dbd", "$(magnetps_DB)/", "")
dbLoadDatabase("MEBT-010_BMD-CH-006.dbd", "$(magnetps_DB)/", "")
dbLoadDatabase("MEBT-010_BMD-CH-007.dbd", "$(magnetps_DB)/", "")
dbLoadDatabase("MEBT-010_BMD-CH-008.dbd", "$(magnetps_DB)/", "")
dbLoadDatabase("MEBT-010_BMD-CH-009.dbd", "$(magnetps_DB)/", "")
dbLoadDatabase("MEBT-010_BMD-CH-010.dbd", "$(magnetps_DB)/", "")
dbLoadDatabase("MEBT-010_BMD-CH-011.dbd", "$(magnetps_DB)/", "")
updateMenuConvert
#
#
############################################################################
# MEBT Corrector CH-001
############################################################################
epicsEnvSet(PSUNIT, "PwrC-PSCH-001")
epicsEnvSet(PSPORT, "CH-01")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=PS$(PSPORT), IP_ADDR=$(PSCH1_IP), P=MEBT-010:, R=$(PSUNIT):")
#
epicsEnvSet(CONVERSIONTABLE, "MEBT-010_BMD-CH-001")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=MEBT-010:, R=$(PSUNIT):, READCURPV=Cur-R, BPT=$(CONVERSIONTABLE), EGU=T, ADEL=0")
#
#
############################################################################
# MEBT Corrector CH-002
############################################################################
epicsEnvSet(PSUNIT, "PwrC-PSCH-002")
epicsEnvSet(PSPORT, "CH-02")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=PS$(PSPORT), IP_ADDR=$(PSCH2_IP), P=MEBT-010:, R=$(PSUNIT):")
#
epicsEnvSet(CONVERSIONTABLE, "MEBT-010_BMD-CH-002")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=MEBT-010:, R=$(PSUNIT):, READCURPV=Cur-R, BPT=$(CONVERSIONTABLE), EGU=T, ADEL=0")
#
#
############################################################################
# MEBT Corrector CH-003
############################################################################
epicsEnvSet(PSUNIT, "PwrC-PSCH-003")
epicsEnvSet(PSPORT, "CH-03")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=PS$(PSPORT), IP_ADDR=$(PSCH3_IP), P=MEBT-010:, R=$(PSUNIT):")
#
epicsEnvSet(CONVERSIONTABLE, "MEBT-010_BMD-CH-003")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=MEBT-010:, R=$(PSUNIT):, READCURPV=Cur-R, BPT=$(CONVERSIONTABLE), EGU=T, ADEL=0")
#
#
############################################################################
# MEBT Corrector CH-004
############################################################################
epicsEnvSet(PSUNIT, "PwrC-PSCH-004")
epicsEnvSet(PSPORT, "CH-04")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=PS$(PSPORT), IP_ADDR=$(PSCH4_IP), P=MEBT-010:, R=$(PSUNIT):")
#
epicsEnvSet(CONVERSIONTABLE, "MEBT-010_BMD-CH-004")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=MEBT-010:, R=$(PSUNIT):, READCURPV=Cur-R, BPT=$(CONVERSIONTABLE), EGU=T, ADEL=0")
#
#
############################################################################
# MEBT Corrector CH-005
############################################################################
epicsEnvSet(PSUNIT, "PwrC-PSCH-005")
epicsEnvSet(PSPORT, "CH-05")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=PS$(PSPORT), IP_ADDR=$(PSCH5_IP), P=MEBT-010:, R=$(PSUNIT):")
#
epicsEnvSet(CONVERSIONTABLE, "MEBT-010_BMD-CH-005")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=MEBT-010:, R=$(PSUNIT):, READCURPV=Cur-R, BPT=$(CONVERSIONTABLE), EGU=T, ADEL=0")
#
#
############################################################################
# MEBT Corrector CH-006
############################################################################
epicsEnvSet(PSUNIT, "PwrC-PSCH-006")
epicsEnvSet(PSPORT, "CH-06")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=PS$(PSPORT), IP_ADDR=$(PSCH6_IP), P=MEBT-010:, R=$(PSUNIT):")
#
epicsEnvSet(CONVERSIONTABLE, "MEBT-010_BMD-CH-006")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=MEBT-010:, R=$(PSUNIT):, READCURPV=Cur-R, BPT=$(CONVERSIONTABLE), EGU=T, ADEL=0")
#
#
############################################################################
# MEBT Corrector CH-007
############################################################################
epicsEnvSet(PSUNIT, "PwrC-PSCH-007")
epicsEnvSet(PSPORT, "CH-07")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=PS$(PSPORT), IP_ADDR=$(PSCH7_IP), P=MEBT-010:, R=$(PSUNIT):")
#
epicsEnvSet(CONVERSIONTABLE, "MEBT-010_BMD-CH-007")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=MEBT-010:, R=$(PSUNIT):, READCURPV=Cur-R, BPT=$(CONVERSIONTABLE), EGU=T, ADEL=0")
#
#
############################################################################
# MEBT Corrector CH-008
############################################################################
epicsEnvSet(PSUNIT, "PwrC-PSCH-008")
epicsEnvSet(PSPORT, "CH-08")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=PS$(PSPORT), IP_ADDR=$(PSCH8_IP), P=MEBT-010:, R=$(PSUNIT):")
#
epicsEnvSet(CONVERSIONTABLE, "MEBT-010_BMD-CH-008")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=MEBT-010:, R=$(PSUNIT):, READCURPV=Cur-R, BPT=$(CONVERSIONTABLE), EGU=T, ADEL=0")
#
#
############################################################################
# MEBT Corrector CH-009
############################################################################
epicsEnvSet(PSUNIT, "PwrC-PSCH-009")
epicsEnvSet(PSPORT, "CH-09")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=PS$(PSPORT), IP_ADDR=$(PSCH9_IP), P=MEBT-010:, R=$(PSUNIT):")
#
epicsEnvSet(CONVERSIONTABLE, "MEBT-010_BMD-CH-009")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=MEBT-010:, R=$(PSUNIT):, READCURPV=Cur-R, BPT=$(CONVERSIONTABLE), EGU=T, ADEL=0")
#
#
############################################################################
# MEBT Corrector CH-010
############################################################################
epicsEnvSet(PSUNIT, "PwrC-PSCH-010")
epicsEnvSet(PSPORT, "CH-10")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=PS$(PSPORT), IP_ADDR=$(PSCH10_IP), P=MEBT-010:, R=$(PSUNIT):")
#
epicsEnvSet(CONVERSIONTABLE, "MEBT-010_BMD-CH-010")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=MEBT-010:, R=$(PSUNIT):, READCURPV=Cur-R, BPT=$(CONVERSIONTABLE), EGU=T, ADEL=0")
#
#
############################################################################
# MEBT Corrector CH-011
############################################################################
epicsEnvSet(PSUNIT, "PwrC-PSCH-011")
epicsEnvSet(PSPORT, "CH-11")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=PS$(PSPORT), IP_ADDR=$(PSCH11_IP), P=MEBT-010:, R=$(PSUNIT):")
#
epicsEnvSet(CONVERSIONTABLE, "MEBT-010_BMD-CH-011")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=MEBT-010:, R=$(PSUNIT):, READCURPV=Cur-R, BPT=$(CONVERSIONTABLE), EGU=T, ADEL=0")
#
#
#- Call iocInit to start the IOC
iocInit()
